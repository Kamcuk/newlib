set -e
/usr/bin/aclocal --force
/usr/bin/autoconf -f
/usr/bin/automake -ac
/bin/rm -rf autom4te.cache
